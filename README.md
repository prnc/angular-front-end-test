# Angular Front-end test documentation

This test is specially prepared for ABC to be qualify as a front-end developer. 

#### Pre-Requisites: Following software / NPM packages must be installed
- Node JS (Latest)
- NPM (Node Package Manager - Latest)
- Angular CLI (Version 6.0.8)
- Gulp CLI (Version 2.1.0)

To execute this test successfully, Please fire following commands one by one.

```sh
$ cd front-end-test // Please write correct full path of directory where you have this test source code located.
$ npm install
$ gulp --production
```

To start angular server, Please run following command from this project directory and navigate to http://localhost:4200/

```sh
$ ng serve
```

***NOTE***: Output screenshot is already added to project root directory. Please check ***Output_Screenshot.png*** file

To execute test, Please fire following command from this project directory and it will automatically open a browser tab with test result.

```sh
$ ng test
```