import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
declare const $:any;

@Injectable({
  providedIn: 'root'
})
export class ApiService {
	constructor(private http:HttpClient){}

	/**********************************************************************
	* Get chart data API
	**********************************************************************/
	getChartData (): Observable<any> {
		return this.http.get('https://raw.githubusercontent.com/ranpariyaLab/dummy-api/master/db.json')
		.pipe(catchError(this.handleError));
	}

	/**********************************************************************
	* Exception(s) Handling function
	**********************************************************************/
	handleError(error) {
		let errorMessage:any = '';
		if (`${error.status}` == '400') {
			if(error.error.status) {
				errorMessage = {
					status: error.error.status,
					error: error.error.error
				}
			} else {
				errorMessage = error.error.error;
			}
		} else if(`${error.status}` == '500' && error && error.error && error.error.error) {
			errorMessage = error.error.error;
		} else if(`${error.status}` == '404') {
			errorMessage = 'Opps! Something went wrong please try again later.';
		} else {
			errorMessage = {'status':error.status, 'message':(error &&  error.error && error.error.error ? error.error.error : 'Opps! Something went wrong please try again later.')};
		}
		return throwError(errorMessage);
	}
}
