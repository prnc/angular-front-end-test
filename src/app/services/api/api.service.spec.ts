import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { ApiService } from './api.service';

describe('ApiService', () => {
	var httpClientSpy,
		service;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports:[ HttpClientTestingModule ],
			providers: [ApiService]
		});
		httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
		service = TestBed.get(ApiService);
	});

	it('should be created', inject([ApiService], (service: ApiService) => {
		expect(service).toBeTruthy();
	}));

	// Should return user data response
	it('should return expected users data response(HttpClient called once)', () => {
		service.getChartData().subscribe(response => {
			expect(response.data.length).toBeGreaterThan(0)
			expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
		});
	});
});
