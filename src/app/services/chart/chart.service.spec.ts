import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ChartService } from './chart.service';

describe('ChartService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpClientTestingModule],
			providers: [ChartService]
		});
	});

	it('should be created', inject([ChartService], (service: ChartService) => {
		expect(service).toBeTruthy();
	}));
});
