import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ApiService } from '../../services/api/api.service';

@Injectable({
	providedIn: 'root'
})
export class ChartService {

	data:any=[];
	colorX = '#d49d9d';
	colorY = '#753434';
	colorZ = '#388e2b';
	constructor(private apiService : ApiService) { }

	private subject = new Subject<any>();
	private subjectApi = new Subject<any>();
	private subjectData = new Subject<any>();
	private subjectColor = new Subject<any>();

	// Sync chart colors from color picker
	sendColor(type, color) {
		console.log(color);
		this.subjectColor.next({type,color});
	}

	getColor(): Observable<any> {
		return this.subjectColor.asObservable();
	}

	// Sync chart type from selectize
	sendIsDonutType(type: boolean) {
		this.subject.next({ text: type });
	}

	getIsDonutType(): Observable<any> {
		return this.subject.asObservable();
	}

	// Sync data from selectize
	sendDataUpdate() {
		this.subjectData.next(this.data);
	}

	getDataUpdate(): Observable<any> {
		return this.subjectData.asObservable();
	}

	// Fetch chart data
	getData(){
		this.apiService.getChartData().subscribe((response:any)=>{
			this.data = response.data;
			this.sendDataUpdate();
		},error=>{
			this.data = {};
		})
	}
}
