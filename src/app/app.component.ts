import { Component } from '@angular/core';
import { ChartService } from './services/chart/chart.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {

	data;
	subscription: Subscription;

	constructor(
		private chartService: ChartService
	){
		// Get notified when data is loaded to initiate other components
		this.subscription = this.chartService.getDataUpdate().subscribe(data => {
			this.data = data;
		});
	}

	ngOnInit() {}
}
