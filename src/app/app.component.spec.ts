import { TestBed, async } from '@angular/core/testing';

// Component
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ChartComponent } from './components/chart/chart.component';
import { HeaderComponent } from './components/header/header.component';
import { PickerComponent } from './components/picker/picker.component';
import { SelectizeComponent } from './components/selectize/selectize.component';

// Modules
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { HttpClientModule } from '@angular/common/http';
import { ColorPickerModule } from 'ngx-color-picker';
import { RouterTestingModule } from '@angular/router/testing'

describe('AppComponent', () => {
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports:[
				FormsModule,
				NgSelectModule,
				HttpClientModule,
				ColorPickerModule,
				RouterTestingModule
			],
			declarations: [
				AppComponent,
				HomeComponent,
				ChartComponent,
				HeaderComponent,
				PickerComponent,
				SelectizeComponent
			],
		}).compileComponents();
	}));
	it('should create the app', async(() => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	}));
});
