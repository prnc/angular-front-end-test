import { Component, OnInit } from '@angular/core';
import { ChartService } from '../../services/chart/chart.service';

@Component({
	selector: 'app-picker',
	templateUrl: './picker.component.html',
	styleUrls: ['./picker.component.css']
})
export class PickerComponent implements OnInit {

	// Colors
	colorX;
	colorY;
	colorZ;

	constructor(public chartService: ChartService) {

		// Sync with chart service
		this.colorX = chartService.colorX;
		this.colorY = chartService.colorY;
		this.colorZ = chartService.colorZ;
	}

	ngOnInit() {}
}
