import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ColorPickerModule } from 'ngx-color-picker';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PickerComponent } from './picker.component';

describe('PickerComponent', () => {
	var component,
		fixture;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports:[ 
				ColorPickerModule, 
				HttpClientTestingModule 
			],
			declarations: [ PickerComponent ]
		})
		.compileComponents();

		fixture = TestBed.createComponent(PickerComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();

	}));

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
