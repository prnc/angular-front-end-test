import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { SelectizeComponent } from './selectize.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SelectizeComponent', () => {
	var component,
		fixture;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports:[
				HttpClientTestingModule,
				NgSelectModule,
				FormsModule
			],
			declarations: [ SelectizeComponent ]
		})
		.compileComponents();

		fixture = TestBed.createComponent(SelectizeComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	}));

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	// Should have two options
	it('should have two options', async(() => {
		expect(component.options.length).toBe(2);
	}));
});
