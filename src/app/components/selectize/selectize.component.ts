import { Component, OnInit } from '@angular/core';
import { ChartService } from '../../services/chart/chart.service';

@Component({
	selector: 'app-selectize',
	templateUrl: './selectize.component.html',
	styleUrls: ['./selectize.component.css']
})
export class SelectizeComponent implements OnInit {

	chartType;
	// Chart Options
	options:any=[{
		    "name": "Donut Chart",
		    "value": "donut"
		},{
		    "name": "Pie Chart",
		    "value": "pie"
		}
	];

	constructor(
		private chartService: ChartService
	){}

	ngOnInit() {
		this.chartType = "donut";
	}

	// Change Chart Type and generate the chart
	onSelectChange(){
		this.chartService.sendIsDonutType(this.chartType == 'donut');
	}
}
