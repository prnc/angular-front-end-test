import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ChartComponent } from './chart.component';

describe('ChartComponent', () => {
	var component,
		fixture;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ HttpClientTestingModule ],
			declarations: [ ChartComponent ]
		})
		.compileComponents();

		fixture = TestBed.createComponent(ChartComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	}));

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	// The colors should be present
	it('should have color data', async(() => {
		expect(component.colorX).toBeTruthy();
		expect(component.colorY).toBeTruthy();
		expect(component.colorZ).toBeTruthy();
	}));

	// SVG should be rendered
	it('should set pie chart', async(() => {
		spyOn(component, 'setChart');
		component.ngOnInit();
		expect(component.setChart).toHaveBeenCalled();
	}));

	// SVG should be rendered
	it('should render SVG', async(() => {
		const fixture = TestBed.createComponent(ChartComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector('svg')).toBeTruthy();
	}));

	// Table should be rendered
	it('should render Table', async(() => {
		const fixture = TestBed.createComponent(ChartComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector('table')).toBeTruthy();
	}));
});
