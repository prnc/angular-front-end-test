import { Component, OnInit } from "@angular/core";
import { ChartService } from '../../services/chart/chart.service';
import { ApiService } from '../../services/api/api.service';
import { Subscription } from 'rxjs';
import * as D3 from "d3";

@Component({
	selector: 'app-chart',
	templateUrl: './chart.component.html',
	styleUrls: ['./chart.component.css']
})

export class ChartComponent implements OnInit {

	colorX;
	colorY;
	colorZ;
	isDonutChart:boolean=true;
	subscription: Subscription;
	apiSubscription: Subscription;
	colorSubscription: Subscription;

	constructor(
		private apiService: ApiService,
		public chartService: ChartService
	){
		// Set default Color
		this.colorX = chartService.colorX;
		this.colorY = chartService.colorY;
		this.colorZ = chartService.colorZ;

		// Update Chart on chart type change
		this.subscription = this.chartService.getIsDonutType().subscribe(message => {
			this.isDonutChart = message.text || false;
			this.setChart();
		});

		// Update Chart on api call
		this.apiSubscription = this.chartService.getDataUpdate().subscribe(result => {
			this.setChart();
		});

		// Update Chart on color change
		this.colorSubscription = this.chartService.getColor().subscribe(result => {
			switch(result.type){
				case 'one' : this.colorX = result.color;break;
				case 'two' : this.colorY = result.color;break;
				case 'three' : this.colorZ = result.color;break;
			}
			this.setChart();
		});
	}

	ngOnInit(){
		this.setChart();
	}

	// Javascript to add Pie Chart
	setChart(){
		if (this.isDonutChart) { D3.select("#pieChart").selectAll("*").remove(); }

		// D3 pie chart
		var data = this.chartService.data,
			width = 400,
			height = 400,
			radius = Math.min(width, height) / 2,
			color = D3.scaleOrdinal()
					.range([this.colorX,this.colorY,this.colorZ]),
			arc = D3.arc()
			.outerRadius(radius - 10)
			.innerRadius(this.isDonutChart?100:0);

		var pie = D3.pie()
			.sort(null)
			.value(function(d) { return d.population; });

		var svg = D3.select("#pieChart")
			.attr("viewBox", '0 0 400 400')
			.append("g")
			.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

		data.forEach(function(d) {
			d.population = +d.population;
		});

		var g = svg.selectAll(".arc")
			.data(pie(data))
			.enter().append("g")
			.attr("class", "arc");

		g.append("path")
			.attr("d", arc)
			.style("fill", function(d) { return color(d.data.age); });

		g.append("text")
			.attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
			.attr("dy", ".35em")
			.style("text-anchor", "middle")
			.text(function(d) { return d.data.age; });
	}
}