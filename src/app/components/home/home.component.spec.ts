import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
	let component: HomeComponent;
	let fixture: ComponentFixture<HomeComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ HttpClientTestingModule ],
			declarations: [ HomeComponent ]
		})
		.compileComponents();

		fixture = TestBed.createComponent(HomeComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	}));

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	// Button must be rendered
	it('should render button "Get Chart"', async(() => {
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector('button').textContent).toContain('Get Chart Data');
	}));

	// Button must be clicked
	it('should click "Get Chart" button', () => {
		const component = fixture.componentInstance; 
		spyOn(component, 'getChartData');
		var el = fixture.debugElement.nativeElement.querySelector('button').click();
		expect(component.getChartData).toHaveBeenCalled();
	});
});
