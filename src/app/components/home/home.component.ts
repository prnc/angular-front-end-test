import { Component, OnInit } from '@angular/core';
import { ChartService } from '../../services/chart/chart.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	constructor(
		private chartService:ChartService
	){}

	ngOnInit() {}

	// Get Chart Data
	getChartData(){
		this.chartService.getData();
	}

}
