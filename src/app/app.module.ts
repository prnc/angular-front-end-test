import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing/app-routing.module';

// Modules
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { HttpClientModule } from '@angular/common/http';
import { ColorPickerModule } from 'ngx-color-picker';

// Components
import { HomeComponent } from './components/home/home.component';
import { ChartComponent } from './components/chart/chart.component';
import { HeaderComponent } from './components/header/header.component';
import { PickerComponent } from './components/picker/picker.component';
import { SelectizeComponent } from './components/selectize/selectize.component';


@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		ChartComponent,
		HeaderComponent,
		PickerComponent,
		SelectizeComponent
	],
	imports: [
		FormsModule,
		CommonModule,
		BrowserModule,
		NgSelectModule,
		HttpClientModule,
		AppRoutingModule,
		ColorPickerModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }