var gulp = require('gulp'),
	del = require('del'),
	gulpsync = require('gulp-sync')(gulp),
	less = require('gulp-less'),
	header = require('gulp-header'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	util = require('gulp-util'),
	notify = require("gulp-notify"),
	cleanCSS = require('gulp-clean-css');

var jsBanner = [  
	'/************************************************************',
	'* Minified Javascript',
	'* All kind of basic Javascription transmission.',
	'*',
	'************************************************************/',
	'\n'].join('\n');

var cssBanner = [  
	'/************************************************************',
	'* Minified CSS for',
	'* All kind of basic css.',
	'*',
	'************************************************************/',
	'\n'].join('\n');

var path = {
	js: {
		src: './src/assets/js/**/*.js',
		dest: './src/assets/dist/js'
	},
	css: {
		src: './src/assets/less/**/*.less',
		dest: './src/assets/dist/css'
	}
}

// --
// Environments
var production = util.env.production;

// --
// Error handler
function onError(err) {
	console.log(err);
	this.emit('end');
}

// --
// Clean Directory
gulp.task('cleanDir', function() {
	return del(["./src/assets/dist"]); 
})

// --
// Watch task
gulp.task('compile-watch', function() {
	gulp.watch(path.css.src, gulpsync.sync(['css-minify' ]));
	gulp.watch(path.js.src, gulpsync.sync(['js-minify']));
});

// --
// Js minify
gulp.task('js-minify', function() {
	return gulp.src(path.js.src)
		.pipe(production ? uglify().on('error', onError) : util.noop())
		.pipe(rename({suffix: '.min'}))
		.pipe(header(jsBanner))
		.pipe(gulp.dest(path.js.dest));
});
// --
// Less compile and minify css
gulp.task('css-minify', function() {
	return gulp.src(path.css.src)
		.pipe(less().on('error', onError))
		.pipe(rename({suffix: '.min'}))
		.pipe(header(cssBanner))
		.pipe(production ? cleanCSS().on('error', onError) : util.noop())
		.pipe(gulp.dest(path.css.dest));
});

gulp.task('default', gulpsync.sync(['cleanDir', 'js-minify','css-minify']));
gulp.task('watch', gulpsync.sync(['cleanDir', 'js-minify','css-minify', 'compile-watch']));